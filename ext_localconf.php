<?php

use Phipfel\PhipfelsWatchword\Controller\WatchwordController;
use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;
use TYPO3\CMS\Core\Imaging\IconRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

defined('TYPO3') || die('Access denied.');

ExtensionUtility::configurePlugin(
	'phipfels_watchword',
	'Displaywatchword',
    [
        WatchwordController::class => 'list',
    ],
	// non-cacheable actions
    [
        WatchwordController::class => 'list',
    ],
);

$iconRegistry = GeneralUtility::makeInstance(IconRegistry::class);

$iconRegistry->registerIcon(
    'phipfels_watchword_icon',
    SvgIconProvider::class,
    ['source' => 'EXT:phipfels_watchword/Resources/Public/Icons/watchword.svg']
);
